# Adaptive_Corr_Filter_Tracking
tracking a moving object in degraded video from a moving platform (i.e., a drone)

I wish I had been aware of this method before! In my LK_OptFlow_tracker repository, you can see the results I got when trying to design my own detector + tracker using optical flow. It is a complicated problem in the case of a moving platform poor features to detect. Plus it is much slower since detection is performed over the whole image in each frame.  

The implementation of the tracker I use to produce the output below can be found here https://github.com/opencv/opencv/blob/master/samples/python/mosse.py, and the algorithms inventors' paper is here http://www.cs.colostate.edu/~draper/papers/bolme_cvpr10.pdf.  

Below is shown output of this program using some longwave infrared video as input. I manually selected the object to track, the vehicle, with a bounding box in the first frame.

![Alt text](acf.gif?raw=true "Output")
